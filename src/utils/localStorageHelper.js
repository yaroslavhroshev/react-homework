export const saveItemsToLocalStorage = (key, state) => {
    localStorage.setItem(key, JSON.stringify(state))
}


export const getItemsFromLocalStorage = (key) => {
    const localStorageValue = localStorage.getItem(key)
    if (localStorageValue) {
        return JSON.parse(localStorageValue)
    }
    return null
}

export const getItemFromLocalStorage = (key, id) => {
    const localStorageValue = JSON.parse(localStorage.getItem(key))
    if (localStorageValue) {
        const localStorageItem = localStorageValue.find(item => item.id === id)
        return localStorageItem
    }
    return null
}

export const changeIsFavouriteFromLocalStorage = (key, id) => {
    const localStorageValue = JSON.parse(localStorage.getItem(key))
    if (localStorageValue) {
        const localStorageItem = localStorageValue.find(item => item.id === id)
        localStorageItem.isFavourite = !localStorageItem.isFavourite
        saveItemsToLocalStorage(key, localStorageValue)
    }
    return null
}

export const changeCountItemFromLocalStorageItems = (key, id, modalMark) => {
    const localStorageValue = JSON.parse(localStorage.getItem(key))
    if (localStorageValue) {
        const localStorageItem = localStorageValue.find(item => item.id === id)
        if (modalMark === 'addedModal') {
            localStorageItem.count++
        } else if (modalMark === 'deleteModal') {
            localStorageItem.count = localStorageItem.count === 0 ? 0 : localStorageItem.count - 1;
        }
        saveItemsToLocalStorage(key, localStorageValue)
    }
    return null
}

// export const countCleaningAfterPurchaseFromLocalStorage = (key, cartArr) => {
//     const localStorageValue = JSON.parse(localStorage.getItem(key));
//     console.log(localStorageValue)
//     if (localStorageValue) {
//         const filteredLocalStorageValue = localStorageValue.filter(item => {
//             return cartArr.some(cartItem => cartItem.id === item.id)
//         }).map(item => {
//             item.count = 0;
//             return item
//         })
//         const newLocalStorageValue = localStorageValue.map(item => {
//             let newItem = {};
//             filteredLocalStorageValue.forEach(filteredItem => {
//                 newItem = filteredItem.id === item.id ? filteredItem : item;
//             })
//             return newItem
//         })
//         console.log(newLocalStorageValue)
//     }
// }

export const countCleaningAfterPurchaseFromLocalStorage = (key, cartArr) => {
    const localStorageValue = JSON.parse(localStorage.getItem(key));
    if (localStorageValue) {
        const newLocalStorageValue = localStorageValue.map(item => {
            item.count = cartArr.some(cartItem => cartItem.id === item.id) ? 0 : item.count;
            return item
        })
        saveItemsToLocalStorage(key, newLocalStorageValue)
    }
}




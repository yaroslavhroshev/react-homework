import React from "react";
import {PatternFormat} from "react-number-format";
import PropTypes from "prop-types";
import {useField} from "formik";
import styles from "./PatternFormatInput.module.scss";

const PatternFormatInput = ({type, ...props}) => {
  const [field, meta] = useField(props.name);
  const {error, touched} = meta;

  return (
    <div className={styles.container}>
      <label htmlFor={props.id}>{props.label || field.name}</label>
      <PatternFormat
        displayType="input"
        format="(###) ### ## ##"
        mask={"#"}
        allowEmptyFormatting
        {...props}
        {...field}
      />

      {touched && error && <p className={styles.errorMesagge}>{error}</p>}
    </div>
  );
};

PatternFormatInput.propTypes = {
  type: PropTypes.string,
};

PatternFormatInput.defaultProps = {
  type: "number",
};

export default PatternFormatInput;

import React from "react";
import PropTypes from "prop-types";
import {useField} from "formik";
import styles from "./CustomInput.module.scss";

const CustomInput = ({type, ...props}) => {
  const [field, meta] = useField(props.name);
  const {error, touched} = meta;

  return (
    <div className={styles.container}>
      <label htmlFor={props.id}>{props.label || field.name}</label>
      <input type={type} {...props} {...field} />

      {touched && error && <p className={styles.errorMesagge}>{error}</p>}
    </div>
  );
};

CustomInput.propTypes = {
  type: PropTypes.string,
};

CustomInput.defaultProps = {
  type: "text",
};

export default CustomInput;

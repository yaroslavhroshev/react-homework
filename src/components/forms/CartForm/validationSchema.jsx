import {object, string, number} from "yup";

export const validationSchema = object({
  name: string().required("Name is required"),
  lastName: string().required("Last Name is required"),
  age: number()
    .required("Age is required")
    .typeError("Age must be a number")
    .positive("The number must be positive")
    .integer("The number must be an integer"),
  address: string().required("Deliver adress is required"),
  phone_number: string()
    .required("Phone number is required")
    .matches(/^\(\d{3}\) \d{3} \d{2} \d{2}$/, "Invalid number format"),
});

import React from "react";
import {Form, Formik} from "formik";
import CustomInput from "../../CustomInput/CustomInput";
import {validationSchema} from "./validationSchema";
import styles from "./CartForm.module.scss";
import {useDispatch, useSelector} from "react-redux";
import {countCleaningAfterPurchaseFromLocalStorage} from "../../../utils/localStorageHelper";
import {ITEMS_KEY} from "../../../assets/constants/keys";
import {getItemsForCounterFromLocalStorage} from "../../../redux/cart/actionCreators";
import PatternFormatInput from "../../PatternFormatInput/PatternFormatInput";

const CartForm = () => {
  const initialValues = {
    name: "",
    lastName: "",
    age: "",
    address: "",
    phone_number: "",
  };

  const dispatch = useDispatch();
  const cart = useSelector((state) => state?.cart?.cart);

  const onSubmit = (values, actions) => {
    const userInfo = `
    User Info:
    Name: ${values.name}
    Last Name: ${values.lastName}
    age: ${values.age}
    deliver address: ${values.address}
    phone number: ${values.phone_number}

    Purchased goods:
    ${cart
      .map((item) => {
        return `
    Product name: ${item.name}
    Count: ${item.count}
    `;
      })
      .join("")}`;

    console.log(userInfo);

    countCleaningAfterPurchaseFromLocalStorage(ITEMS_KEY, cart);
    dispatch(getItemsForCounterFromLocalStorage());
    actions.resetForm();
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
    >
      {({isValid}) => (
        <Form className={styles.container}>
          <CustomInput
            name="name"
            type="text"
            placeholder="Enter your name"
            id="name"
            label="Name"
          />
          <CustomInput
            name="lastName"
            type="text"
            placeholder="Enter your last name"
            id="name"
            label="Last Name"
          />
          <CustomInput
            name="age"
            type="text"
            placeholder="Enter your age"
            id="name"
            label="Age"
          />
          <CustomInput
            name="address"
            type="text"
            placeholder="Enter your deliver adress"
            id="name"
            label="Delivery adress"
          />
          {/* <CustomInput
            name="phone_number"
            type="text"
            placeholder="Enter your phone number"
            id="name"
            label="Phone number"
          /> */}
          <PatternFormatInput
            name="phone_number"
            type="number"
            placeholder="Enter your phone number"
            id="name"
            label="Phone number"
          />

          <button
            className={styles.button}
            disabled={!isValid || cart.length === 0}
            type="submit"
          >
            Checkout
          </button>
        </Form>
      )}
    </Formik>
  );
};

export default CartForm;
